import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username="";
  password="";
  constructor() { }

  ngOnInit() {
  }

  onLogin(){
    if(this.username=="" || this.password=="")
    {
      alert('Enter all Fields');
      this.onCancel();
    }
    else
    {
      if(this.username=='test' && this.password=='test')
      {
        alert('Welcome');
        this.onCancel();
      }
      else
      {
        alert('Enter correct credentials');
        this.onCancel();
      }
    }
  }

  onCancel()
  {
    this.username="";
    this.password=""; 
  }

}
